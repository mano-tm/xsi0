import os
import click

# from outputter.py import canvas()
# from outputter.py import draw_board()
# from colorama import init()
# init()


def check_line(game_list, offset=0):
    flag = True
    test = game_list[0 + offset]
    for i in range(0, 3):
        if game_list[i] == ' ':
            flag = False
            break
        if game_list[i + offset] != test:
            flag = False
            break
    return flag


def check_column(game_list, offset=0):
    flag = True
    test = game_list[0 + offset]
    for i in range(0, 9, 3):
        if game_list[i] == ' ':
            flag = False
            break
        if game_list[i + offset] != test:
            flag = False
            break
    return flag


def check_primary(game_list):
    flag = True
    test = game_list[0]
    for i in range(0, 10, 4):
        if game_list[i] == ' ':
            return False
        if game_list[i] != test:
            return False
    return True


def check_secondary(game_list):
    test = game_list[2]
    for i in range(2, 7, 2):
        if game_list[i] == ' ':
            return False
        if game_list[i] != test:
            return False
    return True


def still_playing(game_list):
    for i in range(0, 3):
        test = check_line(game_list, i)
        if not test:
            test = check_column(game_list, i)
            if not test:
                test = check_primary(game_list)
                if not test:
                    test = check_secondary(game_list)
                    if not test:
                        return True
                    return False
                return False
            return False
        return False


def get_coords(game_list):
    x, y = 0, 0
    while True:
        try:
            x, y = int(input("X: ")), int(input("Y: "))
            if not (0 < x < 4 and 0 < y < 4):
                raise OverflowError
            else:
                break
        except ValueError:
            print("Incercati din nou!")
        except OverflowError:
            print("Introduceti numai numere intre 1 si 3")
    if y == 1:
        return x - 1
    elif y == 2:
        return x + 2
    elif y == 3:
        return x + 5


def unscramble(a):
    if str(a) == 'True':
        return 'O'
    elif str(a) == 'False':
        return 'X'
    elif a == ' ':
        return ' '
    else:
        return "ERRVAL"


def draw_board(game_list):
    print('-' * 7)
    print('|' + unscramble(game_list[0]) + '|' + unscramble(game_list[1]) + '|' + unscramble(game_list[2]) + '|')
    print('|' + unscramble(game_list[3]) + '|' + unscramble(game_list[4]) + '|' + unscramble(game_list[5]) + '|')
    print('|' + unscramble(game_list[6]) + '|' + unscramble(game_list[7]) + '|' + unscramble(game_list[8]) + '|')
    print('-' * 7)


def main():
    board = [' '] * 9
    active_player = False
    still_here = True
    print("Welcome to our game! Player one starts.")
    while still_here:
        click.clear()
        draw_board(board)
        print(active_player + 1)
        t = get_coords(board)
        if board[t] == ' ':
            board[t] = active_player
            active_player = not active_player
        else:
            print("Hey! That spot's already taken!")
            continue
        still_here = still_playing(board)
    active_player = not active_player
    print("Great job, Player " + str(int(active_player)+1) + ", you win!")

main()
