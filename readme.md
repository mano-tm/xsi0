# Hah! A simple Tic-tac-toe program!

## Ver 0.2
* Cleaned up some code and fixed verifications
* Input isn't checked as of now
* Program uses os.system("cls"), making it incompatible with *nix systems
* Some code could be cleaned up
* Can't really print anything, only debug data

~Alex

## Ver 0.3
* Hey! You can now actually see what you're playing! Awesome
* Program now checks input. Shocker that it didn't before, I know...
* Stay tuned for something real special coming next... :)

~Alex